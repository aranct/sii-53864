# changelog

### [v5.2] - 2021/01/04
práctica 5
#### Added:
* Comunicación entre mundo y servidor mediante sockets a través de la clase Socket.
#### Removed
* tuberías (FIFOs) que conectaban el mundo y el cliente. 

### [v4.1] - 2020/12/02
práctica 4
#### Added:
* estructura de cliente servidor en el juego. Servidor conectado con el logger y envía datos al cliente a través de una FIFO.
#### Removed:
* Mundo.cpp
* Mundo.h
* tenis.cpp

### [v3.1] - 2020/11/20
práctica 3
#### Added:
* Funcionamiento del marcador de puntuaciones.
* Bot del jugador 1.
* Fin del juego al llegar a los 3 puntos.

### [v2.2] - 2020/10/29
práctica 2
#### Added:
* Movimiento espera y raqueta.
* Creación del README.
* Cambio tamaño de la pelota.

### [v1.1] - 2020/10/08
práctica 1
#### Modified:
* Cambio código principal.

### [v1.0] - 2020/10/13
práctica 0
#### Added:
* Nombre añadido a la cabecera de los ficheros.
